import React, {Component} from 'react'
import {Route, NavLink, Switch, HashRouter as Router} from "react-router-dom";
import Table from '../Components/Table'
import './Home.scss'

import * as mttl_data from '../data/MTTL.min.json';
import * as ttl_data from '../data/TTLs.min.json';
import * as metrics_data from '../data/metrics.min.json';

let addKSAT = "https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Add+KSAT"
let modKSAT = "https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Modify+KSAT"
let bugReport = "https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Bug"

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { width: 0, height: 0 };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);  
    }
    
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    get_all_table_links(data, url) {
        let nav_list = [];
        for(let [key] of Object.entries(data)) {
            if (key === 'MTTL') {
                continue;
            }
            nav_list.push(
                <li key={key.toLowerCase()}>
                    <NavLink exact to={`${url}/${key.toLowerCase()}`} activeClassName='is-active'>{key}</NavLink>
                </li>
            );
        }
        return nav_list;
    }

    render() {
        let my_ttl_data = ttl_data.default;
        my_ttl_data['MTTL'] = mttl_data.default;
        const match = this.props.match;
        const url = match.url === "/" ? "" : match.url
        const links_list = this.get_all_table_links(my_ttl_data, url);
        

        return (
            <Router basename={this.props.page_url}>
            <div>
                <div className="feedbackButtonGroup">
                    <a className="feedbackButton" target="_blank" rel="noopener noreferrer" href={bugReport}>Report a Bug</a>
                    <a className="feedbackButton" target="_blank" rel="noopener noreferrer" href={addKSAT}>Add KSAT</a>
                    <a className="feedbackButton" target="_blank" rel="noopener noreferrer" href={modKSAT}>Modify KSAT</a>
                </div>
                <div className="taskListTables">
                    <div className="tableSelect">
                        <ul>
                            <li><NavLink exact to={url} activeClassName='is-active'>MTTL</NavLink></li>
                            {links_list}
                        </ul>
                        <a id="viewSource" href="https://gitlab.com/90cos/mttl"><i className="fa fa-gitlab" aria-hidden="true"></i> View Code Repo</a>
                    </div>
                    <div className="tableContent">
                        <Switch>
                            <Route 
                                exact path="/"
                                render={(props) => <Table
                                    data={my_ttl_data} 
                                    metrics={metrics_data.default} 
                                    rowStyle={true}
                                    columns="mttl"
                                    hidden_cols={['_id', 'proficiency', 'comments']}
                                    {...props} 
                                />}
                            />
                            <Route 
                                path={`${url}/:tableId`}
                                render={(props) => <Table 
                                    data={my_ttl_data} 
                                    metrics={metrics_data.default} 
                                    rowStyle={true}
                                    columns="mttl"
                                    hidden_cols={['_id', 'requirement_src', 'requirement_owner', 'work-roles/specializations', 'eval_links', 'parent', 'children', 'comments']}
                                    {...props} 
                                />}
                            />
                        </Switch>
                    </div>      
                </div>
            </div>
            </Router>
        );
    }
}

export default Home;
