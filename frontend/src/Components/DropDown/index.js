import React, {Component} from 'react'
import './DropDown.css'

class DropDown extends Component {
    render() {
        return(
            <div className="my-dropdown" style={this.props.dropStyle}>
                <button className="my-dropbtn">{this.props.name}</button>
                <div className="my-dropdown-content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default DropDown;
