import React from 'react';
import TableToPrint from '../RoadmapNodeTable';

class CollapsibleBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemList: props.itemList //this is the same list as training: role.courses in roadmap
        };
    }
    render() {
        const { node } = this.props;
        return (
            <div>
                <TableToPrint style={this.props.style} itemList={this.state.itemList} node={node} ref={el => (this.componentRef = el)} />
            </div>
        );
    }
}

export default CollapsibleBox
