import React from 'react';
import { DefaultPortLabel } from './DefaultPortLabelWidget';
import CollapsibleBox from '../../CollapsibleBox/index.js';
import '../../node.css';
import DownloadFile from '../../PDFComponent'
import PDFs from '../../PDFIndex.js';

export class DefaultNodeWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.node.name,
      itemList: props.node.itemList,
      isHidden: true,
      topLeft: React.createRef(),
      viewer: props.node.viewer
    };
  }

  handleIsHidden = () => {
    this.setState({isHidden: !this.state.isHidden});
  }

  onRemove() {
    const { node, diagramEngine } = this.props;
    node.remove();
    diagramEngine.forceUpdate();
  }

  getInPorts() {
    const { node } = this.props;
    return node.getInPorts().map((port, i) => <DefaultPortLabel key={i} model={port} />);
  }

  getOutPorts() {
    const { node } = this.props;
    return node.getOutPorts().map((port, i) => <DefaultPortLabel key={i} model={port} />);
  }

  render() {
    const { node } = this.props;
    const numCourses = this.state.itemList.length;
    const nodeDiv = {
      display: "flex",
      flexDirection: "column",
      background: node.color,
      padding: "4px"
    };
    const buttonRow = {
      display: "flex",
      flexDirection: "row",
      paddingBottom: "6px"
    };
    const portsRow = {
      display: "flex",
      flexDirection: "row"
    };
    const buttonStyle = {
      backgroundColor: node.color,
      outline: "none"
    };

    var ojtRef
    if (PDFs[this.state.name]){
      ojtRef = <DownloadFile workrole={this.state.name}/>
    }

    return (
      <div className='basic-node' style={nodeDiv}>
        <div style={buttonRow}>
          <button 
            // Apply default button styles
            className="node-button"
            // Reset the color to the node's color
            style={buttonStyle}
            type="button"
            onClick={this.handleIsHidden.bind(this)}
          >
            {this.state.name.replace(/[-_]/g, " ")}
          </button>
        </div>
        <div className='ports' style={portsRow}>
          <div className='in'>
            {this.getInPorts()}
          </div>
          <label className="course_num">{numCourses} {numCourses === 1 ? "Item" : "Items"}</label>
          
          {ojtRef}

          <CollapsibleBox // this is hidden always for the react-to-print
              style={{display: 'none'}}
              key={this.state.name} 
              name={"Test"} 
              node={node} 
              itemList={this.state.itemList}
              ref={el => (this.componentRef = el)}
          />

          <div className='out'>
            {this.getOutPorts()}
          </div>
        </div>
        <div>
          {
            !this.state.isHidden && <CollapsibleBox 
                                        key={this.state.name} 
                                        name={"Test"} 
                                        node={node} 
                                        itemList={this.state.itemList} 
                                        ref={el => (this.componentRef = el)}
                                    />
          }
        </div>
      </div>
    );
  }
}

