import React, {Component} from 'react'
import RoadMap from './RoadMap';
import './index.scss'
import '../Components/diagram/sass.scss'

let roadmapHelp = "https://90cos.gitlab.io/mttl/documentation/Training-Roadmap.html"

class Roadmap extends Component {
    div_outer = {
        display: "flex",
        flexDirection: "column",
        textAlign: "center",
        height: "150vh",
        margin: "auto"
    }

    render() {
        return (
            <div>
                <h1>Training Roadmap</h1>
                <p className="helpPrompt">Roadmap <a target="_blank" rel="noopener noreferrer" href={roadmapHelp}>Help Documentation</a></p>
                <div style={this.div_outer}>
                    <RoadMap/>
                </div>
            </div>
        );
    }
}

export default Roadmap;