import React from 'react';
import data from '../data/data.json';
import { 
  DiagramEngine,
  DiagramWidget,
  DefaultNodeFactory,
  DefaultLinkFactory, 
  DiagramModel,
  DefaultNodeModel,
  DefaultPortModel,
  LinkModel
} from '../Components/diagram/main';

export class RoadMap extends React.Component {
  discSizes = {};
  nodes = [];
  links = [];
  ports = [];
  phaseColors = ['rgb(0, 192, 255)','rgb(89, 179, 0)','rgb(242, 135, 13)','rgb(255, 0, 0)']

  constructor(props) {
    super(props);
    this.xSize = 200;
    this.ySize = 100;
    this.Yspacing = 40;
    this.Xspacing = 70;
    this.horizontalOffset = 60;
    this.verticalOffset = 40;
    // Setup the diagram engine
    this.engine = new DiagramEngine();
    this.engine.registerNodeFactory(new DefaultNodeFactory());
    this.engine.registerLinkFactory(new DefaultLinkFactory());
    // Setup the diagram model
    this.model = new DiagramModel();
    
    this.setDiscSizes();
    this.setDiscSpacing();
  }

  setDiscSizes() {
    Object.keys(data).forEach(discipline => {
      data[discipline].phases.forEach(phase =>{
        if(discipline in this.discSizes){
          this.discSizes[discipline] = Math.max(this.discSizes[discipline], phase.length)
        }
        else{
          this.discSizes[discipline] = phase.length;
        }
        phase.forEach(role =>{
          if(role.linksTo.length > 0){
          role.linksTo.forEach(element => {
            this.links.push({from: (role.id + "-out"), to: (element + "-in")});
          })};
        });
      });
    });
  }

  setDiscSpacing() {
    let discSpacing = 0;
    Object.keys(data).forEach(discipline => {
      let discMax = this.discSizes[discipline] * this.ySize + this.discSizes[discipline] * this.Yspacing;
      data[discipline].phases.forEach((phase, i) => {
        phase.forEach((role, j) => {
          let align = 0;
          if (phase.length < this.discSizes[discipline]) {
            align = (discMax - phase.length * (this.ySize + this.Yspacing)) / phase.length;
          }
          this.makeNode(role, i, j, discSpacing, discMax, align);
        });
      });
      discSpacing += discMax;
    });
  }

  createNode(options) {
    const { name, color, phase, nodePos, training, viewer, x, y } = options;
    var node = new DefaultNodeModel(name, color, phase, nodePos, training, viewer);
    node.x = x;
    node.y = y;
    node.phase = phase;
    node.nodePos = nodePos;
    node.itemList = training;
    node.viewer = viewer;
    return node;
  }

  createPort(node, options) {
    const { isInput, id, name } = options;
    return node.addPort(new DefaultPortModel(isInput, id, name));
  }

  linkNodes(port1, port2) {
    const link = new LinkModel();
    link.setSourcePort(port1);
    link.setTargetPort(port2);
    return link;
  }

  makeNode(role, phaseCounter, nodeCounter, discSpacing, discMax, align) {
    //how many alignment correct segments to pad to the node
    let alignment = (align * (nodeCounter + 1) - (this.ySize / 2)) / 2;
    const node1 = this.createNode({
      name: role.id,
      color: phaseCounter < this.phaseColors.length ? this.phaseColors[phaseCounter] : this.phaseColors[0],
      phase: phaseCounter + 1,
      nodePos: this.nodes.length + 1,
      training: role.courses,
      viewer: this.updateView,
      x: this.xSize * phaseCounter + this.Xspacing * phaseCounter + this.horizontalOffset,
      y: this.ySize * nodeCounter + this.Yspacing * nodeCounter + this.verticalOffset + alignment + discSpacing,
    });
    const port1in = this.createPort(node1, {
      isInput: true,
      id: role.id + "-in",
    });
    const port1out = this.createPort(node1, {
      isInput: false,
      id: role.id + "-out",
    });
    this.nodes.push(node1);
    this.ports[node1.name + "-in"] = port1in;
    this.ports[node1.name + "-out"] = port1out;
    return null;
  }

  render() {
    this.nodes.forEach(element => {
      this.model.addNode(element);
    });
    this.links.forEach(element => {
      this.model.addLink(this.linkNodes(this.ports[element.from], this.ports[element.to]));
    });
    // Load the model into the diagram engine
    this.engine.setDiagramModel(this.model);
    // Render the canvas
    return <DiagramWidget diagramEngine={this.engine} actions={{zoom: false, deleteItems: false, moveItems: false}}/>;
  }
}

export default RoadMap;