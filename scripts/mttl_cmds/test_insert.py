#!/usr/bin/python3

import os
import argparse
import unittest
import subprocess
import pymongo
import json
import random
from find import find_query
from bson.objectid import ObjectId
from insert import insert_ksat_data, insert_rel_link_data, get_new_ksat_key

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links


class TestMethods(unittest.TestCase):
    def test_insert_ksat(self):
        # make sure it does not exist already
        self.assertEqual(len(find_query(reqs, 'ksat', {'ksat_id': get_new_ksat_key(reqs, 'knowledge')}, None)), 0)
        test_data = {
            'ksat_type': 'knowledge',
            'description': 'This is a test description',
            'topic': 'test',
            'parent': [
                {
                    '$oid': '5f457f1e1ea90ba9adb32a9a'
                }
            ],
            'requirement_src': [],
            'requirement_owner': "",
            'comments': 'test comments'
        }
        with open('test.json', 'w') as wrfile:
            json.dump(test_data, wrfile, sort_keys=False, indent=4)
        cmd = 'python3 mttl.py insert ksat -f test.json'
        subprocess.run(cmd.split())
        
        found_items = list(reqs.find({'description': test_data['description']}))
        # make sure it now exist
        self.assertEqual(len(found_items), 1)
        self.assertEqual(found_items[0]['description'], 'This is a test description')
        self.assertEqual(found_items[0]['topic'], 'test')
        self.assertEqual(str(found_items[0]['parent'].pop(0)), test_data['parent'][0]['$oid'])
        self.assertEqual(found_items[0]['comments'], 'test comments')

    def test_insert_trn_rel_link(self):
        # make sure it does not exist already
        self.assertEqual(len(find_query(rls, 'rel-link', {'$and': [{'course': 'test_course'}, {'module': 'my_module'}]}, None, False)), 0)
        test_data = [{
            'course': 'test_course',
            'module': 'my_module',
            'topic': 'test_topic',
            'subject': 'test_subject',
            'KSATs': [
                {
                    'ksat_id': {
                        '$oid': '5f457f1e1ea90ba9adb32a9a'
                    },
                    'item_proficiency': 'D',
                    'url': 'https://my.test.url'
                }
            ],
            'network': 'test_network',
            'work-roles': [],
            'topic_path': 'this/path',
            'map_for': 'training'
        }]
        with open('test.json', 'w') as wrfile:
            json.dump(test_data, wrfile, sort_keys=False, indent=4)
        cmd = 'python3 mttl.py insert rel-link -f test.json'
        subprocess.run(cmd.split())
        
        found_items = list(rls.find({'$and': [{'course': 'test_course'}, {'module': 'my_module'}]}))
        # make sure it now exist
        self.assertEqual(len(found_items), 1)
        self.assertEqual(found_items[0]['course'], 'test_course')
        self.assertEqual(found_items[0]['module'], 'my_module')
        self.assertEqual(found_items[0]['topic'], 'test_topic')
        self.assertEqual(found_items[0]['subject'], 'test_subject')
        for i, item in enumerate(test_data[0]['KSATs']):
            self.assertEqual(str(found_items[0]['KSATs'][i]['ksat_id']), item['ksat_id']['$oid'])
            self.assertEqual(found_items[0]['KSATs'][i]['item_proficiency'], item['item_proficiency'])
            self.assertEqual(found_items[0]['KSATs'][i]['url'], item['url'])
        self.assertEqual(found_items[0]['network'], 'test_network')

    def test_insert_evl_rel_link(self):
        # make sure it does not exist already
        self.assertEqual(len(find_query(rls, 'rel-link', {'$and': [{'question_id': 'test_question'}, {'test_id': 'test_id'}]}, None, False)), 0)
        test_data = [{
            'test_id': 'CCD',
            'test_type': 'knowledge',
            'question_name': 'test_question',
            'question_id': 'test_question',
            'topic': 'test_topic',
            'language': 'test_lang',
            'question_proficiency': 'D',
            'complexity': 0,
            'estimated_time_to_complete': 0,
            'KSATs': [
                {
                    'ksat_id': {
                        '$oid': '5f457f1e1ea90ba9adb32a9a'
                    },
                    'item_proficiency': 'D',
                    'url': 'https://my.test.url'
                }
            ],
            'network': 'test_network',
            'work-roles': [],
            'map_for': 'eval'
        }]
        with open('test.json', 'w') as wrfile:
            json.dump(test_data, wrfile, sort_keys=False, indent=4)
        cmd = 'python3 mttl.py insert rel-link -f test.json'
        subprocess.run(cmd.split())

        found_items = list(rls.find({'$and': [{'question_id': 'test_question'}, {'test_id': 'CCD'}]}))
        # make sure it now exist
        self.assertEqual(len(found_items), 1)
        self.assertEqual(found_items[0]['test_id'], 'CCD')
        self.assertEqual(found_items[0]['test_type'], 'knowledge')
        self.assertEqual(found_items[0]['question_id'], 'test_question')
        self.assertEqual(found_items[0]['language'], 'test_lang')
        self.assertEqual(found_items[0]['question_proficiency'], 'D')
        self.assertEqual(found_items[0]['complexity'], 0)
        self.assertEqual(found_items[0]['estimated_time_to_complete'], 0)
        for i, item in enumerate(test_data[0]['KSATs']):
            self.assertEqual(str(found_items[0]['KSATs'][i]['ksat_id']), item['ksat_id']['$oid'])
            self.assertEqual(found_items[0]['KSATs'][i]['item_proficiency'], item['item_proficiency'])
            self.assertEqual(found_items[0]['KSATs'][i]['url'], item['url'])
        self.assertEqual(found_items[0]['network'], 'test_network')


if __name__ == "__main__":
    unittest.main()