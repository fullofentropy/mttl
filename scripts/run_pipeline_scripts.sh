#!/bin/bash

rm -rf frontend/src/data
./scripts/before_script.sh

./scripts/pre_build/create_mttl_children.py
./scripts/pre_build/create_mttl_links.py
./scripts/build/build_data.py Roadmap.json -o frontend/src/data
./scripts/build/create_mttl_dataset.py
./scripts/build/create_ttl_dataset.py
./scripts/build/create_extra_datasets.py
./scripts/build/create_metrics.py