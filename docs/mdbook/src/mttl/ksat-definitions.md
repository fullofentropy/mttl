# KSAT Definitions

*The following are definitions for **Knowledge, Skill, and Ability** according to National Initiative for Cybersecurity Education (NICE) Cybersecurity Workforce Framework (NCWF).*

**Knowledge** is a body of information applied directly to the performance of a function.

**Skill** is often defined as an observable competence to perform a learned psychomotor act.
Skills in the psychomotor domain describe the ability to physically manipulate a tool or
an instrument like a hand or a hammer. Skills needed for cybersecurity rely less on physical
manipulation of tools and instruments and more on applying tools, frameworks, processes.

A skill can be measured objectively via test.  (e.g. Did they find a key, compile correctly, pass unit tests, etc.)

**Ability** is the competence to perform an observable behavior or a behavior that results in an
observable product.

An ability requires a subjective determination to properly evaluate (e.g. OJT, Scenario, etc.)

*US Cyber Command's Cyberspace Standards Analyst Team (CSAT) defined a **Task** as:*

**Task** A Succession of work activities used to produce a distinguishable and discernable output that can be independently used or consumed. Tasks should align with the roles and responsibilities associated with a work role, function, or position. Task statements should:
- Describe activities (not KSATs)
- Have a certain beginning and end
- Represent activities performed by an individual
- Stated as not too broad nor specific
- Have an identifiable output
