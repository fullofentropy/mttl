# Contributing to the MTTL

Anybody with a 90COS gitlab account can modify or add to the MTTL. Any changes or additions to the MTTL will be reviewed and approved or rejected by a designated Subject Matter Expert (SME) for the associated work role that the changes made apply to.

The MTTL consists of four distinct .json files described in the [MTTL Data Dictionary](https://gitlab.com/90cos/mttl/-/wikis/Home#data-file-directory-structure). An example of a single KSAT definition inside this file is shown and described below. Add new KSATs to these JSON files will require using our development environment or sending in a new KSAT request and to follow the [Change Management Workflow](https://gitlab.com/90cos/mttl/-/wikis/Home#change-management-workflow) documented on the MTTL Home page. 

| Field | Required? | Description |
|-------|-----------|---------|
| _id | required (generated) | The KSAT object `_id`. This will be auto-generated when inserting using the `./mttl.py insert` script |
| ksat_id | required | The KSAT id number. When adding a new KSAT, add to the bottom of the page and use the next available number sequence |
| description | required | Reference [Bloom's Taxonomy](https://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy/) to help write meaningful KSAT descriptions. This taxonomy was created specifically for teaching, learning, and assessment. The taxonomy focuses on using verbs and gerunds to label categories and subcategories. *These “action words” describe the cognitive processes by which thinkers encounter and work with knowledge.* The cognitive process is represented by 6 categories.|
| parent | optional | List all parent `_id` related to this KSAT.  A parent KSAT is typically a broader KSAT that this KSAT supports. This KSAT represents an essential building-block for its parent(s) KSAT. Reference the [KSAT parent-child](ksat-parent-child-rels.md) for more information on this relationship. |
| topic | required | Provide a topic this KSAT supports. |
| requirement_src | required | List one or more requirement documents that originated this KSAT. These are for KSATs originated external to the 90COS |
| requirement_owner | required |Provide the office of primary responsibility (OPR) for this KSAT.  These are for KSATs originated external to the 90COS |
| comments | optional | Provide any additional supporting comments. |
| updated_on | optional | The date this KSAT was last modified.  This is automatically updated by the pipeline |

```json
{
	"_id": {
		"$oid": "5f457f1e1ea90ba9adb32aa2"
	},
	"ksat_id": "K0003",
	"parent": [
		{
			"$oid": "5f457f1e1ea90ba9adb32c9c"
		}
	],
	"description": "Understand the purpose of breaking down requirements to atomic functions.",
	"topic": "Software Engineering fundamentals",
	"requirement_src": [
		"ACC CCD CDD TTL"
	],
	"requirement_owner": "ACC A3/2/6KO",
	"comments": "Making sure that you don't have one massive function that does everything, but breaking down the functions by discrete tasks"
}
```
Current rel-link.json definitions are listed [here](https://90cos.gitlab.io/cyt/training/admin/mttl/rel-link-org.html)